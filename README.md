# Powershell-Notes

[1. What is Powershell ?](https://eros.narola.online/git/Jigneshvaray/powershell-notes/blob/master/1.What%20is%20powershell)<br>
[2. What is Cmdlet ?](https://eros.narola.online/git/Jigneshvaray/powershell-notes/blob/master/2.What%20is%20Cmdlet)<br>
[3. What is pipline ?](https://eros.narola.online/git/Jigneshvaray/powershell-notes/blob/master/3.pipeline)<br>
[4. Basic file and folder operations](https://eros.narola.online/git/Jigneshvaray/powershell-notes/blob/master/4.Basic%20file%20and%20folder%20operations)<br>
[5. Getting Information about System](https://eros.narola.online/git/Jigneshvaray/powershell-notes/blob/master/5.%20Getting%20Information%20about%20System)<br>
[6. Managing Task and Services](https://eros.narola.online/git/Jigneshvaray/powershell-notes/blob/master/6.%20Managing%20Task%20and%20Services)<br>
[7. Networking Command](https://eros.narola.online/git/Jigneshvaray/powershell-notes/blob/master/7.%20Networking%20%20Command)<br>
[8. Wireless command](https://eros.narola.online/git/Jigneshvaray/powershell-notes/blob/master/8.%20Wireless%20command)<br>
[9. Utility Commnads](https://eros.narola.online/git/Jigneshvaray/powershell-notes/blob/master/9.%20Utility%20Commnads)<br>
[10. Powershell ISE](https://eros.narola.online/git/Jigneshvaray/powershell-notes/blob/master/10.%20Powershell%20ISE)<br>
[11. Powershell-data-types](https://eros.narola.online/git/Jigneshvaray/powershell-notes/blob/master/11.%20Powershell-data-types)<br>
[12. Variables in ps](https://eros.narola.online/git/Jigneshvaray/powershell-notes/blob/master/12.%20Variables%20in%20ps)<br>
[13. Operators](https://eros.narola.online/git/Jigneshvaray/powershell-notes/blob/master/13.%20Operators)<br>
[14. If-else](https://eros.narola.online/git/Jigneshvaray/powershell-notes/blob/master/14.%20If-else)<br>
[15. Loops](https://eros.narola.online/git/Jigneshvaray/powershell-notes/blob/master/15.%20Loops)<br>
[]()<br>
[]()<br>
